﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CardGameForm
{
    public partial class GameForm : Form
    {
        public GameForm(Player player, List<Entity> enemies)
        {
            _player = player;
            _enemies = enemies;
            _player.AddCard(new Card());
            _enemies.Add(new Entity(100));
            InitializeComponent();
            label1.DataBindings.Add("Text", player, "HP");
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _player.Deck[0].use(_player, _enemies[0]);
            label2.Text = _enemies[0].HP.ToString();
        }

        private Player _player;
        private List<Entity> _enemies;
    }
}
