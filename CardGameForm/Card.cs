﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGameForm
{
    public class Card
    {
        public Card(int cost, string category, string race, EventArgs e, bool upgrade = false)
        {
            _cost = cost;
            _category = category;
            _race = race;
            _upgrade = upgrade;
            _e = e;
        }

        public int Cost
        {
            get
            {
                return _cost;
            }
        }

        public string Category
        {
            get
            {
                return _category;
            }
        }

        public string Race
        {
            get
            {
                return _race;
            }
        }

        public void use(Player player, Entity entity)
        {
            _eventHandler(this, _e);
        }

        protected int _cost;
        protected string _category;
        protected string _race;
        protected bool _upgrade;
        protected EventArgs _e;
        protected EventHandler _eventHandler;
    }
}
