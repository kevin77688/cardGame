﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGameForm
{
    public class Entity
    {
        public Entity(int hitPoints, int strength = 0, int dexterity = 0, int shield = 0)
        {
            _hitPoints = hitPoints;
            _strength = strength;
            _dexterity = dexterity;
            _sheild = shield;
        }

        public int HP
        {
            get
            {
                return _hitPoints;
            }
            set
            {
                _hitPoints = value;
            }
        }

        public int Strength
        {
            get
            {
                return Strength;
            }
            set
            {
                _strength = value;
            }
        }

        public int Dexterity
        {
            get
            {
                return _dexterity;
            }
            set
            {
                _dexterity = value;
            }
        }

        public int Shield
        {
            get
            {
                return _sheild;
            }
            set
            {
                _sheild = value;
            }
        }

        public void Attacked(int damage, int times = 1, int strength = 0)
        {
            int allDamage = (damage + strength - _dexterity) * times;
            if (_sheild > 0)
            {
                if (_sheild >= allDamage)
                {
                    _sheild -= allDamage;
                }
                else
                {
                    _hitPoints -= allDamage - _sheild;
                    _sheild = 0;
                }
            }
            else
            {
                _hitPoints -= allDamage;
            }
        }

        public void AttackedByRealDamage(int damage, int times = 1)
        {
            _hitPoints -= damage * times;
        }

        protected int _hitPoints;
        protected int _strength;
        protected int _dexterity;
        protected int _sheild;
    }
}
