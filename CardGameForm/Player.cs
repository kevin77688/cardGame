﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGameForm
{
    public class Player : Entity
    {
        public Player(int hitPoints, List<Card> deck, int strength = 0, int dextirity = 0, int shield = 0) : base(hitPoints, strength, dextirity, shield)
        {
            _deck = deck;
        }

        public List<Card> Deck
        {
            get
            {
                return _deck;
            }
        }

        public void AddCard(Card card)
        {
            _deck.Add(card);
        }
        private List<Card> _deck;
    }
}
